/**
 * Creates a new Car object
 * @constructor
 * @param {String} model 
 */
const Car = function(model) {
    this.currentSpeed = 0;
    this.carModel = model;

  
};
Car.prototype.accelerate = function(){
    this.currentSpeed++;
};
Car.prototype.brake = function(){
    if(this.currentSpeed > 0){
        this.currentSpeed--;
    }
};
Car.prototype.toString = function(){
    return `The ${this.carModel} is going ${this.currentSpeed} miles per hour`;
}