$(document).ready(function(){
  let todayList = $('.today-list');
  let laterList = $('.later-list');

  $('ul').on('click',function(event){
    let target = $(event.target);
    if(target.is('span')){
      target.parent().toggleClass('done');
    }
    if(target.is('a')){
      if(target.hasClass('toToday')){
        target.addClass('toLater').removeClass('toToday');
        target.text('Move to Later');
        todayList.append(target.parent());
      }
      else if(target.hasClass('toLater')){
        target.addClass('toToday').removeClass('toLater');
        target.text('Move to Today');
        laterList.append(target.parent());
      }
      else if(target.hasClass('delete')){
        target.parent().remove();
      }
    }
  });
  $('.add-item').on('click',function(event){
    let target = $(event.target);
    let textBox = target.parent().find('input');
    let value = textBox.val();
    let ulElement = target.parent().parent().find('ul');
    let li = "<li></li>";
    let span = "<span>" + value + "</span>";
    
    let aDelete = "<a class='delete'>Delete</a>";

    ulElement.append(li);
    let lastItem = ulElement.find('li').last();
    lastItem.append(span);

    if(ulElement.hasClass('later-list')){
      let aMove = "<a class='move toToday'>Move to Today</a>";
      lastItem.append(aMove);
    }
    else if(ulElement.hasClass('today-list')){
      let aMove = "<a class='move toLater'>Move to Later</a>";
      lastItem.append(aMove);
    }
    lastItem.append(aDelete);
    textBox.val("");
  });
});
